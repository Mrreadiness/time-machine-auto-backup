import datetime
from pathlib import Path

from pydantic import BaseSettings


class Settings(BaseSettings):
    db_path: Path
    target_vpn_config_name: str
    target_time_machine_name: str
    target_start_time: datetime.time = datetime.time(hour=2)
    target_end_time: datetime.time = datetime.time(hour=9)

    class Config:
        env_file = ".env"
