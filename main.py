from config import Settings
from repository.repository import JsonRepository
from time_machine.time_machine import TimeMachine
from use_cases import ProcessBackup
from vpn.wireguard import WireGuardVPN

if __name__ == "__main__":
    settings = Settings()
    with JsonRepository(settings.db_path) as repository:
        vpn = WireGuardVPN(settings.target_vpn_config_name)
        tm = TimeMachine(settings.target_time_machine_name)
        command = ProcessBackup(
            vpn, tm, repository, settings.target_start_time, settings.target_end_time
        )
        command()
