import abc


class IRepository(abc.ABC):
    @abc.abstractmethod
    def get_is_backup_was_triggered_by_script(self) -> bool:
        ...

    @abc.abstractmethod
    def set_is_backup_was_triggered_by_script(self, value: bool) -> None:
        ...
