import json
from pathlib import Path

from repository.interfaces import IRepository


class JsonRepository(IRepository):
    def __init__(self, db_path: Path):
        assert db_path.exists(), "DB file not found"
        self.db_path = db_path
        self.db: dict = {}

    def __enter__(self) -> "JsonRepository":
        with open(self.db_path, mode="r") as db_file:
            data = json.load(db_file)
        assert type(data) is dict, "Wrong DB file structure"
        self.db = data
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        json_object = json.dumps(self.db)
        with open(self.db_path, mode="w") as db_file:
            db_file.write(json_object)

    def get_is_backup_was_triggered_by_script(self) -> bool:
        return self.db.get("need_for_cleanup_after_backup_stop", False)

    def set_is_backup_was_triggered_by_script(self, value: bool) -> None:
        self.db["need_for_cleanup_after_backup_stop"] = value
