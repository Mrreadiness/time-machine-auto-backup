import abc
from enum import Enum


class TimeMachineStatus(Enum):
    running = "running"
    stopped = "stopped"


class ITimeMachine(abc.ABC):
    @property
    @abc.abstractmethod
    def status(self) -> TimeMachineStatus:
        ...

    @abc.abstractmethod
    def start(self) -> None:
        ...

    @abc.abstractmethod
    def stop(self) -> None:
        ...
