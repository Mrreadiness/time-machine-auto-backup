import plistlib
import subprocess
import time
import uuid

from loguru import logger

from time_machine.exceptions import TimeMachineError
from time_machine.interfaces import ITimeMachine, TimeMachineStatus


class TimeMachine(ITimeMachine):
    def __init__(self, name: str):
        destination_id = self._get_destination_id_by_name(name)
        if not destination_id:
            raise TimeMachineError("Destination not found")

        self.destination_id = destination_id

    def start(self) -> None:
        subprocess.run(
            ["tmutil", "startbackup", "-d", str(self.destination_id).upper()],
            stdout=subprocess.PIPE,
        )
        self._wait_for_tm_status(TimeMachineStatus.running)
        logger.info("TimeMachine started")

    def stop(self) -> None:
        subprocess.run(["tmutil", "stopbackup"], stdout=subprocess.PIPE)
        self._wait_for_tm_status(TimeMachineStatus.stopped, 120)
        logger.info("TimeMachine stopped")

    @property
    def status(self) -> TimeMachineStatus:
        process = subprocess.run(["tmutil", "status", "-X"], stdout=subprocess.PIPE)
        running_number: int = plistlib.loads(process.stdout).get("Running", 0)
        return TimeMachineStatus.running if running_number > 0 else TimeMachineStatus.stopped

    @staticmethod
    def _get_destination_id_by_name(name: str) -> uuid.UUID | None:
        process = subprocess.run(["tmutil", "destinationinfo", "-X"], stdout=subprocess.PIPE)
        destinations = plistlib.loads(process.stdout).get("Destinations", list())
        for destination in destinations:
            if destination.get("Name") == name:
                id = destination.get("ID")
                return uuid.UUID(id) if id else None
        return None

    def _wait_for_tm_status(self, status: TimeMachineStatus, timeout: int = 10) -> None:
        """
        :param status: expected status
        :param timeout: timeout in seconds
        :return:
        """
        for _ in range(timeout):
            if self.status == status:
                break
            time.sleep(1)

        if self.status != status:
            raise TimeMachineError("Wrong Time Machine status")
