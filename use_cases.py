import datetime

from loguru import logger

from repository.interfaces import IRepository
from time_machine.interfaces import ITimeMachine, TimeMachineStatus
from vpn.interfaces import IVPN, VPNStatus


class ProcessBackup:
    def __init__(
        self,
        vpn: IVPN,
        tm: ITimeMachine,
        repository: IRepository,
        target_start_time: datetime.time,
        target_end_time: datetime.time,
    ):
        self.vpn = vpn
        self.tm = tm
        self.repository = repository
        self.target_start_time = target_start_time
        self.target_end_time = target_end_time

    def __call__(self) -> None:
        if self._is_time_for_backup():
            if self.tm.status == TimeMachineStatus.running:
                logger.info("TimeMachine is running - shutdown")
                return

            logger.info("It's time for TimeMachine backup - starting")
            self._start_backup()
        else:
            if not self.repository.get_is_backup_was_triggered_by_script():
                logger.info("Last TimeMachine was triggered by user - shutdown")
                return

            if self.tm.status == TimeMachineStatus.stopped:
                logger.info("TimeMachine is stopped - shutdown")
                self._cleanup_after_backup_stop()
            else:
                logger.info("TimeMachine working time expired - stopping")
                self._stop_backup()

    def _is_time_for_backup(self) -> bool:
        now = datetime.datetime.now().time()
        return self.target_start_time <= now <= self.target_end_time

    def _start_backup(self) -> None:
        if self.vpn.status != VPNStatus.connected:
            self.vpn.activate()

        self.tm.start()
        self.repository.set_is_backup_was_triggered_by_script(True)

    def _stop_backup(self) -> None:
        self.tm.stop()
        self._cleanup_after_backup_stop()

    def _cleanup_after_backup_stop(self) -> None:
        if self.vpn.status != VPNStatus.disconnected:
            logger.info("VPN deactivating after last backup")
            self.vpn.deactivate()

        self.repository.set_is_backup_was_triggered_by_script(False)
