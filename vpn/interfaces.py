import abc
import enum


class VPNStatus(enum.Enum):
    connected = "connected"
    connecting = "connecting"
    disconnecting = "disconnecting"
    disconnected = "disconnected"


class IVPN(abc.ABC):
    @property
    @abc.abstractmethod
    def status(self) -> VPNStatus:
        ...

    @abc.abstractmethod
    def activate(self) -> None:
        ...

    @abc.abstractmethod
    def deactivate(self) -> None:
        ...
