import subprocess
import time

from loguru import logger

from vpn.exceptions import VPNError
from vpn.interfaces import IVPN, VPNStatus


class WireGuardVPN(IVPN):
    def __init__(self, config_name: str):
        if not self._vpn_exist(config_name):
            raise ValueError("VNP Doesn't exist")

        self.config_name = config_name

    @property
    def status(self) -> VPNStatus:
        return self._get_status(self.config_name)

    def activate(self) -> None:
        subprocess.run(
            ["/usr/sbin/scutil", "--nc", "start", self.config_name], stdout=subprocess.PIPE
        )
        try:
            self._wait_for_vpn_status(VPNStatus.connected)
            logger.info("WireGuard activated")
        except (AssertionError, VPNError):
            raise VPNError("Unable to connect to vpn")

    def deactivate(self) -> None:
        subprocess.run(
            ["/usr/sbin/scutil", "--nc", "stop", self.config_name], stdout=subprocess.PIPE
        )
        try:
            self._wait_for_vpn_status(VPNStatus.disconnected)
            logger.info("WireGuard deactivated")
        except (AssertionError, VPNError):
            raise VPNError("Unable to disconnect to vpn")

    def _vpn_exist(self, config_name: str) -> bool:
        try:
            self._get_status(config_name)
            return True
        except ValueError:
            return False

    @staticmethod
    def _get_status(config_name: str) -> VPNStatus:
        process = subprocess.run(
            ["/usr/sbin/scutil", "--nc", "status", config_name], stdout=subprocess.PIPE
        )
        result = process.stdout.decode("utf-8").partition("\n")[0].strip()
        match result:
            case "Connected":
                return VPNStatus.connected
            case "Connecting":
                return VPNStatus.connecting
            case "Disconnected":
                return VPNStatus.disconnected
            case "Disconnecting":
                return VPNStatus.disconnecting
            case _:
                raise ValueError("VNP Doesn't exist")

    def _wait_for_vpn_status(self, status: VPNStatus, timeout: int = 10) -> None:
        """
        :param status: expected status
        :param timeout: timeout in seconds
        :return:
        """
        for _ in range(timeout):
            if self.status == status:
                break
            time.sleep(1)

        if self.status != status:
            raise VPNError("Wrong VPN status")
